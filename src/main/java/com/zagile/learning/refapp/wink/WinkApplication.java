/**
 * 
 */
package com.zagile.learning.refapp.wink;


/**
 * @author Julio Mendoza
 *
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Application;

// Jackson imports
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;


public class WinkApplication extends Application {

  /**
   * Get the list of service classes provided by this JAX-RS application
   */
  @Override
  public Set<Class<?>> getClasses() {
    Set<Class<?>> serviceClasses = new HashSet<Class<?>>();
    serviceClasses.add(HashMap.class);
    return serviceClasses;
  }
  
  @Override
  public Set<Object> getSingletons() {
    Set<Object> s = new HashSet<Object>();
    
    // Register the Jackson provider for JSON
    
    // Make (de)serializer use a subset of JAXB and (afterwards) Jackson annotations
    // See http://wiki.fasterxml.com/JacksonJAXBAnnotations for more information
    ObjectMapper mapper = new ObjectMapper();
    AnnotationIntrospector primary = new JaxbAnnotationIntrospector();
    AnnotationIntrospector secondary = new JacksonAnnotationIntrospector();
    AnnotationIntrospector pair = new AnnotationIntrospector.Pair(primary, secondary);
    mapper.getDeserializationConfig().withAnnotationIntrospector(pair);
    mapper.getSerializationConfig().withAnnotationIntrospector(pair);
    
    // Set up the provider
    JacksonJaxbJsonProvider jaxbProvider = new JacksonJaxbJsonProvider();
    jaxbProvider.setMapper(mapper);
    
    s.add(jaxbProvider);
    return s;
  }

}