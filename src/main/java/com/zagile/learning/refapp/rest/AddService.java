/**
 * 
 */
package com.zagile.learning.refapp.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;


/**
 * @author Julio Mendoza
 *
 */

@Path("/extadd")
public class AddService {
    
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_FORM_URLENCODED })
    public Response add(Map<String, Object> input) {
        System.out.println("[PLUGIN] Input: a=" + input.get("a") + ", b=" +  input.get("b") );
        Map<String, Object> output = new HashMap<String, Object>();
        output.put("c",  (Integer) input.get("a") + (Integer) input.get("b") );
        return Response.ok(output).build();
    }
}