package com.zagile.learning.refapp.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class SimpleRestApiModel {

    @XmlElement(name = "value")
    private String message;

    public SimpleRestApiModel() {
    }

    public SimpleRestApiModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}