package com.zagile.learning.refapp.rest;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */

@Path("/root")
public class SimpleRestApi {
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
       return Response.ok(new SimpleRestApiModel("Hello, I'm a simple rest api")).build();
    }
    @Path("/add")
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_FORM_URLENCODED })
    public Response getMessage(Map<String, Object> input) {
        System.out.println("[PLUGIN] Input: a=" + input.get("a") + ", b=" +  input.get("b") );
        Map<String, Object> output = new HashMap<String, Object>();
        output.put("c",  (Integer) input.get("a") + (Integer) input.get("b") );
        return Response.ok(output).build();
    }
}