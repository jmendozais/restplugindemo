package com.zagile.learning.refapp;

public interface MyPluginComponent
{
    String getName();
}