package ut.com.zagile.learning.refapp;

import org.junit.Test;
import com.zagile.learning.refapp.MyPluginComponent;
import com.zagile.learning.refapp.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}