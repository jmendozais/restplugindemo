package ut.com.zagile.learning.refapp.rest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.zagile.learning.refapp.rest.AddService;
import com.zagile.learning.refapp.rest.SimpleRestApi;
import com.zagile.learning.refapp.rest.SimpleRestApiModel;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;

public class AddServiceTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }
    
    @Test
    public void addTest() {
        AddService resource = new AddService();
        Map<String, Object> request = new HashMap<String, Object>();
        request.put("a", new Integer(2));
        request.put("b", new Integer(5));
        Response response = resource.add(request);
        final Map<String, Object> message = (Map<String, Object>) response.getEntity();
        System.out.println("[CLIENT] Response: " + message.get("c"));
        assertEquals("wrong result: " + message.get("c"), new Integer(7), message.get("c"));
    }
}
