package ut.com.zagile.learning.refapp.rest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import com.zagile.learning.refapp.rest.SimpleRestApi;
import com.zagile.learning.refapp.rest.SimpleRestApiModel;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;

public class SimpleRestApiTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
        SimpleRestApi resource = new SimpleRestApi();

        Response response = resource.getMessage();
        final SimpleRestApiModel message = (SimpleRestApiModel) response.getEntity();

        assertEquals("wrong message","Hello, I'm a simple rest api",message.getMessage());
    }
    @Test
    public void addTest() {
        SimpleRestApi resource = new SimpleRestApi();
        Map<String, Object> request = new HashMap<String, Object>();
        request.put("a", new Integer(2));
        request.put("b", new Integer(5));
        Response response = resource.getMessage(request);
        final Map<String, Object> message = (Map<String, Object>) response.getEntity();
        System.out.println("[CLIENT] Response: " + message.get("c"));
        assertEquals("wrong result: " + message.get("c"), new Integer(7), message.get("c"));
    }
}
