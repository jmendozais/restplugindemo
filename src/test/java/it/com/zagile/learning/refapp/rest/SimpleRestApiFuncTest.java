package it.com.zagile.learning.refapp.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.core.MediaType;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import com.zagile.learning.refapp.rest.SimpleRestApi;
import com.zagile.learning.refapp.rest.SimpleRestApiModel;
import com.zagile.learning.refapp.wink.WinkApplication;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class SimpleRestApiFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }
    
    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/simplerestapi/1.0/root";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        SimpleRestApiModel message = resource.get(SimpleRestApiModel.class);

        assertEquals("wrong message","Hello, I'm a simple rest api",message.getMessage());
    }
    @Test
    public void addTest() {
        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/simplerestapi/1.0/root/add";
        
        ClientConfig config = new ClientConfig();
        config.applications(new WinkApplication());
        
        RestClient client = new RestClient(config);
        Resource resource = client.resource(resourceUrl)
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON);
        Map<String, Object> request = new HashMap<String, Object>();
        request.put("a", new Integer(2));
        request.put("b", new Integer(5));
        
        final Map<String, Object> message = resource.post(Map.class, request);
        assertEquals("wrong result: " + message.get("c"), new Integer(7), message.get("c"));
    }
}
